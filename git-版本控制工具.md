# 版本控制工具-git

## 1. 集中式与分布式

![1631499405364](assets/1631499405364.png)

## 2. git的使用

### git的入门

~~~markdown
1. 自报家门
	# 如果用户名不存在 可以随便写
	git config --global user.email "你的邮箱"
	git config --global user.name "你的用户名"
	
2. 初始化版本库
	# 选择合适的项目目录去初始化
	git init
	
3. 将文件添加到git管理
	git add 文件名		将文件添加到git的版本库
	git commit 文件名	将文件进行提交到git的版本库
		# 此时会弹出vim界面  添加本次修改的版本说明
	git commit -m "版本说明" 将文件进行提交到git的版本库 （推荐）
	
4. 查看git的历史提交记录
	git log
		commit 0dd08eecc78fc4ad6011d9001aa93ecaf2285c97 版本id
        Author: maoxy-python <maoxy@zparkhr.com.cn>		提交者
        Date:   Mon Sep 13 10:32:33 2021 +0800			时间
        创建了user.java文件并在文件中添加了第一个controller	  版本说明
	git log --pretty=oneline	以简洁的形式查看历史记录	推荐
	
5. 仓库状态
	git status
		红色：代表文件发生了修改，但还未添加到版本库  git add
		绿色：代表文件修改已经添加到了版本库  但没生成版本号 git commit
		nothing to commit, working tree clean 当前分支不需要操作
		
6. 版本回退
	先查看版本号 git log --pretty=oneline
	git reset --hard 版本号
	
7. 显示所有的操作记录
	git reflog
	再使用版本回退命令进行回退
~~~

### 仓库的说明

![1631503666773](assets/1631503666773.png)

### git的撤销修改

~~~markdown
1. 撤销工作区的修改
	git checkout -- 文件名
2. 撤销暂存区的修改
	a.将暂存区的修改拉回到工作区
		git reset HEAD 文件名
	b. 使用命令撤回工作区的修改	
		git checkout -- 文件名
3. 一旦提交到分支 无法彻底删除
~~~

### git的删除

~~~markdown
1. 删除文件
	rm 文件名|目录名  此时删除的是工作区的文件
2. 确定删除
	git rm 文件名
	git commit -m “版本说明”
	
3. 撤销删除
	git checkout -- 文件名  撤销工作区的删除
~~~



## 3. git的分支

​	![1631505087584](assets/1631505087584.png)

~~~markdown
1. 查看分支
	git branch
	
2. 创建分支
	git branch 分支名
	git checkout -b 分支名  创建并切换分支
	
3. 切换分支
	git checkout 分支名
	
4. 删除分支
	git branch -d 分支名
	
5. 合并分支
	git merge 分支名
		向谁合并需要切换到该分支上执行此命令
		
6. 合并分支时如果某个文件了发生了冲突，则解决冲突后再次提交
	编辑发生冲突的文件  vi xxx
	git add
	git commit 
~~~

## 4. 远程仓库

> `github`，`gitee=码云`

### 使用方式

~~~markdown
$ mkdir cf
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/chang-fuxin/cf.git
git push -u origin master


1. 在github|码云注册自己的账号

2. 创建远程仓库
		https://gitee.com/maoxy-python/ems2104.git
		
3. 将本地版本库与远程仓库进行绑定
	git remote add origin https://gitee.com/chang-fuxin/ems.git仓库地址
	
4. 查看绑定的远程仓库地址
	git remote -v
	
5. 将本地版本库推送到远程仓库上
	 git push --set-upstream origin master	绑定远程仓库后第一次推送时使用
	git push  向远程仓库推送代码
	
6. 移除远程仓库地址
	git remote remove origin
    
7. ssh免密访问码云
	a. 绑定ssh地址
		git remote add origin ssh地址
	b. 在本机生成公私钥钥对  执行命令后连续enter四次
		ssh-keygen -t rsa -C "注册github|码云邮箱"
	c. 在本机目录下找到公钥 并将公钥文件的内容拷贝至码云|github
		C:\Users\504教师机\.ssh  
			id_rsa.pub	公钥文件
	d. 在码云|github中添加公钥
		用户头像-->设置-->ssh公钥--添加公钥
		
8. 克隆项目
	git clone https项目地址
	
9. 推送自己的分支 切换到该分支上
	git push origin 分支名
	git push --set-upstream origin dev 第一次在该分支上推送
	
10. 远程仓库冲突问题解决
	a. 先将冲突从远程仓库拉取到本地
		git pull
	b. 在本地编辑需要发生冲突的文件
		vi ..
	c. 将修改后的结果提交
		git add  git commit 
		git push
~~~



## 5. idea集成git

### idea终端切换git bash

~~~markdown
# 终端切换为git bash后出现编码问题
	修改git安装目录下的bash.bashrc文件 添加以下两行
	D:\Program Files\Git\etc\bash.bashrc
        export LANG="zh_CN.UTF-8"
        export LC_ALL="zh_CN.UTF-8"
~~~

![1631522867573](assets/1631522867573.png)

### .ignore忽略文件

> 项目中有些文件不需要被git管理，如.idea，有些文件不能上传到远程仓库,如项目的配置。可以使用忽略文件将该文件进行忽略，一旦忽略，该文件或文件夹将不会被git进行管理

![1631524141604](assets/1631524141604.png)

- 创建.gitignore

> 写在该文件中的目录或文件夹不会被git管理

![1631524243503](assets/1631524243503.png)





# 作业

~~~markdown
1. 掌握git命令的使用
2. 掌握git在idea中的使用
~~~







